import axios from "axios";
import Header from "@/components/Header/Header.vue";
import Loader from "@/components/Loader/Loader.vue";
import { defineComponent } from "vue";
import type { Character } from "@/interfaces";
import { useCharactersStore } from "@/stores/characters";

export default defineComponent({
	name: "CharacterDetail",
	components: {
		Header,
		Loader,
	},
	setup() {
		const charactersStore = useCharactersStore();

		return { charactersStore };
	},
	data() {
		return {
			character: {} as Character,
		};
	},
	computed: {
		currentCharacter() {
			return this.charactersStore.currentCharacter;
		},

		loading() {
			return this.charactersStore.loading.characterDetail;
		},
	},
	mounted() {
		this.getCharacterDetail();
	},
	methods: {
		async getCharacterDetail() {
			const params = this.$route.params;
			const id = parseInt(params.id as string);
			const characters = this.charactersStore.characters;
			/**
			 * By default, we use data in local store
			 */
			this.character = characters.find((c: { id: number }) => c.id === id) as Character;
			/**
			 * But if the user reloads the page without going through the character list
			 * Or if characters list is empty
			 */
			if (!this.character) {
				const url = `https://rickandmortyapi.com/api/character/${id}`;
				const response = await axios.get(url);
				this.character = response.data;
			}

			this.charactersStore.setLoading({
				loadingKey: "characterDetail",
				value: false,
			});
		},
	},
});
