import { apiUrl } from "@/main";
import { useCharactersStore } from "@/stores/characters";
import axios from "axios";
import swal from "sweetalert";
import { defineComponent } from "vue";

export default defineComponent({
	name: "Pagination",
	setup() {
		const charactersStore = useCharactersStore();

		return { charactersStore };
	},
	computed: {
		hasNext() {
			return this.charactersStore.pagination.next;
		},
		hasPrev() {
			return this.charactersStore.pagination.prev;
		},
		currentPage() {
			return parseInt(this.$route.query.page as string);
		},
		pagesCount() {
			return this.charactersStore.pagination.pages;
		},
		q() {
			return this.charactersStore.query;
		},
	},
	methods: {
		getPages(c: number, m: number) {
			const current = c;
			const last = m;
			const delta = 2;
			const left = current - delta;
			const right = current + delta + 1;
			const range = [];
			const rangeWithDots = [];
			let l;

			for (let i = 1; i <= last; i++) {
				if (i == 1 || i == last || (i >= left && i < right)) range.push(i);
			}
			for (const i of range) {
				if (l) {
					if (i - l === 2) rangeWithDots.push(l + 1);
					else if (i - l !== 1) rangeWithDots.push("...");
				}
				rangeWithDots.push(i);
				l = i;
			}

			return rangeWithDots;
		},

		oopsAlert() {
			return swal({
				title: "Oops!",
				text: "Le filtrage est activé, veuillez videz le champs recherche!",
				icon: "info",
			});
		},

		async paginate(action: "prev" | "next") {
			const q: string = this.charactersStore.query;

			if (q.length > 0) return this.oopsAlert();

			const pagination = this.charactersStore.pagination;
			const query = this.$route.query;
			let page: number;

			if (!pagination[action]) return;
			if (!query.page) query.page = "1";
			if (action === "next") {
				page = parseInt(query.page as string) + 1;
			} else {
				page = parseInt(query.page as string) - 1;
			}

			this.charactersStore.setLoading({ loadingKey: "characters", value: true });
			await axios.get(pagination[action]).then((response) => {
				const data = response.data;
				this.charactersStore.pagination.currentPage = page;

				this.charactersStore.setCharacters(data);
				this.$router.push({ path: "characters", query: { page: page + "" } });
				this.charactersStore.setLoading({ loadingKey: "characters", value: false });
			});
		},

		async goTo(page: number | string) {
			const q: string = this.charactersStore.query;

			if (q.length > 0) return this.oopsAlert();

			const url = `${apiUrl}/character/?page=${page}`;

			this.charactersStore.setLoading({ loadingKey: "characters", value: true });
			await axios.get(url).then((response) => {
				const data = response.data;
				this.charactersStore.pagination.currentPage = page;

				this.charactersStore.setCharacters(data);
				this.$router.push({ path: "characters", query: { page: page + "" } });
				this.charactersStore.setLoading({ loadingKey: "characters", value: false });
			});
		},
	},
});
