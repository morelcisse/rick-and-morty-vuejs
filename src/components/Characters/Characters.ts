import Pagination from "@/components/Pagination/Pagination.vue";
import Header from "@/components/Header/Header.vue";
import Loader from "@/components/Loader/Loader.vue";
import { defineComponent } from "vue";
import { useCharactersStore } from "@/stores/characters";
import { vue3Debounce } from "vue-debounce";

export default defineComponent({
	name: "Characters",
	components: {
		Pagination,
		Header,
		Loader,
	},
	directives: {
		debounce: vue3Debounce({ lock: true }),
	},
	setup() {
		const charactersStore = useCharactersStore();

		return { charactersStore };
	},
	data() {
		return {
			query: "",
			status: ["alive", "dead", "unknown"],
		};
	},
	computed: {
		characters() {
			return this.charactersStore.characters;
		},

		loading() {
			return this.charactersStore.loading.characters;
		},
	},
	mounted() {
		this.setPage();
		this.getCharacters();
	},
	methods: {
		setPage() {
			const queryPage = this.$route.query.page;
			const page = this.charactersStore.pagination.currentPage || parseInt(queryPage as string) || 1;

			if (!queryPage) {
				this.$router.push({
					path: "characters",
					query: { page: page + "" },
				});
			}

			this.charactersStore.pagination.currentPage = page;
		},

		async getCharacters() {
			await this.charactersStore.getCharacters();
		},

		goToCharacterDetail(id: number) {
			this.$router.push(`/characters/${id}`);
		},
	},
});
